Feature 1 (Complete)
    - Fork and clone Project Alpha * 
    - Create/Activate Venv *
    - Upgrade pip *
    - Install django *
    - Install black *
    - Install flake8 *
    - [ ] Install djhtml *
    
Testing for Feature 1 (Complete)
    - python -m unittest tests.test_feature_01

Add and commit your progress (Complete)
    - Add, commit, and push your changes
    - git add .
    - git commit -m "Feature 1 complete"
    - git push origin main

Feature 2 (Complete)
    - In the repository directory:
        - Create Django project named tracker 
        - Create apps named Accounts, Projects, and Tasks. Install them in tracker Django project in INSTALLED_APPS list
        - Run migrations
        - Create a super user

    Add/Commit (Complete)
        - git add .
        - git commit -m "Feature 2 complete"
        - git push origin main

Feature 3 (Complete)
    - Create a Project model within projects Django app *

Testing for Feature 3 (Complete)
    - python manage.py test tests.test_feature_03

Add/Complete 
    - git add .
    - git commit -m "Feature 3 complete"
    - git push origin main
      
Feature 4 
    - Register Project model with admin 

Testing for Feature 4 
    - python manage.py test tests.test_feature_04

Add/Complete 
    - git add .
    - git commit -m "Feature 4 complete"
    - git push origin main
